import { useContext } from 'react';
import { Container, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Banner() {
  const { user } = useContext(UserContext);

  return (
    <>
      <Container className="mt-5 pd-5">
        <h1>Only the best for your bestie.</h1>
        {user.isAdmin ? (
          <></>
        ) : (
          <Link className="btn btn-dark mt-4" to={`/products`}>
            Shop Now!
          </Link>
        )}
      </Container>
      <Container className="fixed-bottom">
        <Image
          className="float-end"
          src="https://media.istockphoto.com/id/1377867397/photo/close-up-portrait-of-beautiful-black-poodle.jpg?s=612x612&w=0&k=20&c=iQ8fAlkWOaTrNrIgdRHsxCTqe_iET14pUSr__2ofsVk="
        ></Image>
      </Container>
    </>
  );
}
