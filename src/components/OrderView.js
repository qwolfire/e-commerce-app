import { useEffect, useState, useContext } from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function OrderView() {
  const { user } = useContext(UserContext);
  const { id } = useParams();
  const [order, setOrder] = useState();
  const [product, setProduct] = useState();

  useEffect(() => {
    const authToken = localStorage.getItem('token');

    const fetchOrder = () => {
      fetch(`https://capstone-2-ecleo.onrender.com/orders/${id}`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setOrder(data);

          fetch(
            `https://capstone-2-ecleo.onrender.com/products/${data.products[0]?.productId}`
          )
            .then((res) => res.json())
            .then((data) => {
              setProduct(data);
            });
        });
    };

    fetchOrder();
  }, [id]);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          <Card className="my-3 col-lg-8 col-md-10 mx-auto">
            <Card.Body>
              {user?.isAdmin ? (
                <>
                  <Card.Title>{`Order Id: ${id}`}</Card.Title>
                  <Card.Subtitle>{`User Id: ${order?.userId}`}</Card.Subtitle>
                  <Card.Subtitle className="mt-4">Items:</Card.Subtitle>
                  <Card.Text className="my-0">{`${product?.name}`}</Card.Text>
                  <Card.Text className="my-0">{`Price: PHP ${product?.price}.00`}</Card.Text>
                  <Card.Text className="my-0">{`Quantity: ${order?.products[0]?.quantity}`}</Card.Text>
                  <Card.Subtitle className="mt-4">{`Total: PHP ${order?.totalAmount}.00`}</Card.Subtitle>
                  <Link
                    className="btn btn-secondary mt-2 float-end"
                    to={`/orders`}
                  >
                    Go back
                  </Link>
                </>
              ) : (
                <>
                  <Card.Title>{`Order Id: ${id}`}</Card.Title>
                  <Card.Subtitle className="mt-4 mb-2">Items:</Card.Subtitle>
                  <Card.Text className="my-0">{`${product?.name}`}</Card.Text>
                  <Card.Text className="my-0">{`Price: PHP ${product?.price}.00`}</Card.Text>
                  <Card.Text className="my-0">{`Quantity: ${order?.products[0]?.quantity}`}</Card.Text>
                  <Card.Subtitle className="mt-4">{`Total: PHP ${order?.totalAmount}.00`}</Card.Subtitle>
                  <Link
                    className="btn btn-secondary mt-2 float-end"
                    to={`/orders`}
                  >
                    Back
                  </Link>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
