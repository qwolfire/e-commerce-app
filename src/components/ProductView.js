import { useContext, useEffect, useState } from 'react';
import {
  Container,
  Card,
  Row,
  Col,
  Button,
  Form,
  InputGroup,
} from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  // stored data type for user validation
  const { id } = useParams();
  const { user } = useContext(UserContext);

  // state hooks to store the values
  const [product, setProduct] = useState();
  const [quantity, setQuantity] = useState(1);

  const authToken = localStorage.getItem('token');
  const navigate = useNavigate();

  const addQuantity = () => {
    setQuantity(quantity + 1);
  };

  const subQuantity = () => {
    setQuantity(quantity - 1);
  };

  const checkOut = () => {
    fetch(`https://capstone-2-ecleo.onrender.com/orders/checkout`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: product?.name,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data !== 'undefined') {
          Swal.fire(
            'Thank You for Ordering!',
            'Wait for email confirmation.',
            'success'
          );
          navigate('/orders');
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
        }
      });
  };

  useEffect(() => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, [id]);

  return (
    <Container className="mt-3">
      <Row className="justify-content-evenly">
        <Col className="col-lg-6">
          <Card className="my-3">
            <Card.Body>
              <Card.Img
                variant="top"
                src="https://cdn.shopify.com/s/files/1/1081/8942/products/DSC06275_color_black_grande.jpg?v=1571438911"
              />
              <Card.Title>{product?.name}</Card.Title>
              <Card.Subtitle className="mt-4">Description:</Card.Subtitle>
              <Card.Text>{product?.description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {product?.price}</Card.Text>
              {user.isAdmin || !user.id ? (
                <></>
              ) : (
                <>
                  <Card.Subtitle className="mt-4">Total:</Card.Subtitle>
                  <Card.Text>Php {product?.price * quantity}</Card.Text>

                  <Card.Subtitle className="mb-2">Quantity:</Card.Subtitle>

                  <InputGroup className="mb-3">
                    <Button
                      variant="outline-secondary"
                      onClick={() => subQuantity()}
                    >
                      -
                    </Button>
                    <Form.Control
                      id="disabledTextInput"
                      placeholder={`${quantity}`}
                      disabled
                    />
                    <Button
                      variant="outline-secondary"
                      onClick={() => addQuantity()}
                    >
                      +
                    </Button>
                  </InputGroup>
                </>
              )}

              {user.id ? (
                <>
                  {user.isAdmin ? (
                    <Link className="btn btn-secondary float-end" to={`/admin`}>
                      Go back
                    </Link>
                  ) : (
                    <>
                      <Link className="btn btn-secondary mt-2" to={`/products`}>
                        Cancel
                      </Link>
                      <Link
                        className="btn btn-dark mt-2 float-end"
                        onClick={() => checkOut()}
                      >
                        Checkout
                      </Link>
                    </>
                  )}
                </>
              ) : (
                <Link className="btn btn-dark float-end" to={`/login`}>
                  Login
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
