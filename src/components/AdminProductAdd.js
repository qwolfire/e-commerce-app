import { useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Add() {
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState();
  const [isActive, setIsActive] = useState(false);

  const authToken = localStorage.getItem('token');
  const navigate = useNavigate();

  const product = {
    name: productName,
    description: description,
    price: price,
  };

  function addProduct(e) {
    e.preventDefault();

    console.log(product);

    fetch(`https://capstone-2-ecleo.onrender.com/products/add`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(product),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire(`${productName} successfully added!`, '', 'success');
          navigate('/admin');
        } else {
          Swal.fire(
            `${productName} already exists!`,
            'Edit product instead.',
            'error'
          );
          navigate('/products/add');
        }
      });

    //   clear inputs
    setProductName('');
    setDescription('');
    setPrice();
  }

  //   To activate submit button
  useEffect(() => {
    if (productName !== '' && description !== '' && price !== 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, price]);

  return (
    <>
      <h1 className="text-center p-3 my-4">ADD NEW PRODUCT</h1>

      <Form
        className="col-md-8 col-lg-4 mx-auto"
        onSubmit={(e) => addProduct(e)}
      >
        <Form.Group className="mb-3" controlId="productName">
          <Form.Label>Product Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Product Name"
            value={productName}
            onChange={(e) => setProductName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Description:</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            type="text"
            placeholder="Enter Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="price">
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Link className="btn btn-secondary mt-4" to={`/admin`}>
          Cancel
        </Link>
        {isActive ? (
          <Button className="float-end mt-4" variant="dark" type="submit">
            Add Product
          </Button>
        ) : (
          <Button
            className="float-end mt-4"
            variant="dark"
            type="submit"
            disabled
          >
            Add Product
          </Button>
        )}
      </Form>
    </>
  );
}
