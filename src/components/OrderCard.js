import { Card, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function OrderCard({ order }){

    const { _id, purchasedOn } = order
    
    return(
        <Card className='my-3 col-lg-8 col-md-10 mx-auto'>
            <Card.Body>
                <Card.Title>Order Id:</Card.Title>
                <Card.Subtitle>{_id}</Card.Subtitle>
                <br/>
                <Row>
                    <div>
                    <Link className='btn btn-dark float-end' to={`/orders/${_id}`}>Details</Link>
                    <Card.Text>{`Purchased on: ${purchasedOn}`}</Card.Text>
                    </div>
                </Row>
            </Card.Body>
        </Card>
    )
};