import { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductCard({ product }) {
  const { _id, name, description, price, isActive } = product;
  const { user } = useContext(UserContext);

  const authToken = localStorage.getItem('token');
  const refresh = () => window.location.reload(true);

  const activateProduct = (e) => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${_id}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire(`${name} activated!`, '', 'success');
          refresh();
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
        }
      });
  };

  const deactivateProduct = (e) => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${_id}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire(`${name} deactivated!`, '', 'success');
          refresh();
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
        }
      });
  };

  return (
    <>
      {!user.isAdmin ? (
        <Card className="m-lg-3 my-3 col-lg-3 col-md-6">
          <Card.Body>
            <Card.Img
              variant="top"
              src="https://cdn.shopify.com/s/files/1/1081/8942/products/DSC06275_color_black_grande.jpg?v=1571438911"
            />
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mt-4">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{`PHP ${price}.00`}</Card.Text>
            <Link className="btn btn-dark float-end" to={`/products/${_id}`}>
              Buy Now
            </Link>
          </Card.Body>
        </Card>
      ) : (
        <Card className="m-lg-3 my-3 col-lg-3 col-md-6">
          <Card.Body>
            <Card.Img
              variant="top"
              src="https://cdn.shopify.com/s/files/1/1081/8942/products/DSC06275_color_black_grande.jpg?v=1571438911"
            />
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>{_id}</Card.Subtitle>
            <Card.Text></Card.Text>
            <Card.Subtitle className="mt-4">Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{`PHP ${price}.00`}</Card.Text>

            <Link className="btn btn-dark" to={`/products/${_id}`}>
              View
            </Link>
            <Link
              className="btn btn-dark float-end"
              to={`/products/${_id}/edit`}
            >
              Update
            </Link>
            {isActive ? (
              <Button
                className="float-end mx-1"
                variant="dark"
                onClick={(e) => deactivateProduct(e)}
              >
                Deactivate
              </Button>
            ) : (
              <Button
                className="float-end mx-1"
                variant="secondary"
                onClick={(e) => activateProduct(e)}
              >
                Activate
              </Button>
            )}
          </Card.Body>
        </Card>
      )}
    </>
  );
}
