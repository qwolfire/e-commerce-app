import { useContext } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="dark" variant="dark" expand="md">
      <Container fluid>
        <Navbar.Brand as={Link} className="px-5" to="/">{`[PP]`}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse
          id="basic-navbar-nav"
          className="justify-content-end px-5"
        >
          <Nav>
            {user.id ? (
              <>
                <Nav.Link
                  as={NavLink}
                  to="/"
                  className="px-4"
                >{`${user?.lastName}, ${user?.firstName}`}</Nav.Link>
                {user.isAdmin ? (
                  <Nav.Link as={NavLink} to="/admin">
                    Dashboard
                  </Nav.Link>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/products">
                      Products
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/orders">
                      Orders
                    </Nav.Link>
                  </>
                )}
                <Nav.Link as={NavLink} to="/logout">
                  Logout
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link className="px-3" as={NavLink} to="/products">
                  Products
                </Nav.Link>
                <Nav.Link className="px-3" as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link className="px-3" as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
