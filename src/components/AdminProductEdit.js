import { useEffect, useState } from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';
import { useNavigate, useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductEdit() {
  const { id } = useParams();
  const [newName, setNewName] = useState();
  const [newDescription, setNewDescription] = useState();
  const [newPrice, setNewPrice] = useState();
  const [currentProduct, setCurrentProduct] = useState();

  const authToken = localStorage.getItem('token');
  const navigate = useNavigate();
  const refresh = () => window.location.reload(true);

  const newProduct = {
    name: newName,
    description: newDescription,
    price: newPrice,
  };

  const updateProduct = (e) => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${id}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newProduct),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire('Product successfully updated!', '', 'success');
          navigate('/admin');
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
          navigate('/admin');
        }
      });
  };

  const activateProduct = (e) => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${id}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire(`${currentProduct.name} activated!`, '', 'success');
          refresh();
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
        }
      });
  };

  const deactivateProduct = (e) => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${id}`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire(`${currentProduct.name} deactivated!`, '', 'success');
          refresh();
        } else {
          Swal.fire('Try Again!', 'Encountered an issue.', 'error');
        }
      });
  };

  useEffect(() => {
    fetch(`https://capstone-2-ecleo.onrender.com/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setCurrentProduct(data);
      });
  }, [id]);

  return (
    <>
      <h1 className="text-center p-3 my-4">EDIT PRODUCT</h1>

      <Form className="col-md-8 col-lg-4 mx-auto">
        <Form.Group className="mb-3" controlId="curProductName">
          <Form.Label>Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder={currentProduct?.name}
            value={newProduct?.name}
            onChange={(e) => setNewName(e.target.value)}
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="curProductDesc">
          <Form.Label>Description:</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            type="text"
            placeholder={currentProduct?.description}
            value={newProduct?.description}
            onChange={(e) => setNewDescription(e.target.value)}
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Row>
          <Form.Group
            as={Col}
            sm="12"
            md="6"
            className="mb-3"
            controlId="curProductPrice"
          >
            <Form.Label>Price:</Form.Label>
            <Form.Control
              type="number"
              placeholder={currentProduct?.price}
              value={newProduct?.price}
              onChange={(e) => setNewPrice(e.target.value)}
            />
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
        </Row>
        {currentProduct?.isActive ? (
          <Button
            className="float-end"
            variant="dark"
            onClick={(e) => deactivateProduct(e)}
          >
            Deactivate
          </Button>
        ) : (
          <Button
            className="float-end"
            variant="secondary"
            onClick={(e) => activateProduct(e)}
          >
            Activate
          </Button>
        )}
        <Button
          className="float-end mx-2"
          variant="dark"
          onClick={(e) => updateProduct(e)}
        >
          Update
        </Button>
        <Link className="btn btn-secondary" to={`/admin`}>
          Cancel
        </Link>
      </Form>
    </>
  );
}
