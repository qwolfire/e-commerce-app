import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  // stored data type for user validation
  const { user, setUser } = useContext(UserContext);

  // state hooks to store the values of the input fields
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // state to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  // connects to backend, res client will see
  function getProfile(e) {
    e.preventDefault();

    fetch(`https://capstone-2-ecleo.onrender.com/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          // login confirmation
          Swal.fire(
            'Welcome to Precious Pooch!',
            'Login Successful',
            'success'
          );
        } else {
          // login fail
          Swal.fire(
            'Authentication Failed',
            'Check your login credentials and try again.',
            'error'
          );
        }
      });

    // clear inputs
    setEmail('');
    setPassword('');
  }

  // retrieves access token to be used for authentication
  const retrieveUserDetails = (token) => {
    fetch(`https://capstone-2-ecleo.onrender.com/users/profile`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          firstName: data.firstName,
          lastName: data.lastName,
          isAdmin: data.isAdmin,
        });
      });
  };

  // to enable submit button
  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <>
      <h1 className="text-center p-3 my-4">LOGIN</h1>

      {user.id ? (
        <Navigate to="/" />
      ) : (
        <Form
          onSubmit={(e) => getProfile(e)}
          className="col-md-8 col-lg-4 mx-auto"
        >
          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address:</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-4" controlId="formBasicPassword">
            <Form.Label>Password:</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          {isActive ? (
            <Button className="float-end" variant="dark" type="submit">
              Login
            </Button>
          ) : (
            <Button className="float-end" variant="dark" type="submit" disabled>
              Login
            </Button>
          )}
        </Form>
      )}
    </>
  );
}
