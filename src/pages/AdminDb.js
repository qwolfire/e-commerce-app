import { useContext, useState, useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCard from "../components/ProductCard"
import UserContext from '../UserContext';


export default function AdminDb(){

    const [product, setProducts] = useState([])
    const { user } = useContext(UserContext);

    useEffect(() => {
        const authToken = localStorage.getItem('token')

        fetch(`https://capstone-2-ecleo.onrender.com/products/all`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {

                return(
                    <ProductCard key={ product._id } product={product} />
                )
            }))
        })
    }, [])

    return(
        <>
            <Container>
                <Row>
                    <Col>
                        <h1 className='mt-5 mb-2'>{`Hello, ${user?.firstName} ${user?.lastName}`}</h1>

                        <Link className="btn btn-dark" to={`/products/add`}>Add New Product</Link>
                        <Link className="btn btn-dark mx-3" to={`/orders`}>View Orders</Link>
                    </Col>
                </Row>
            </Container>

            <Container className='p-3 my-4'>
                <Row className='justify-content-evenly'>
                    { product }
                </Row>
            </Container>
        </>
    )

};