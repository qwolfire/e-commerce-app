import { useEffect, useState } from 'react';
import OrderCard from '../components/OrderCard';

export default function Orders() {
  const [order, setOrders] = useState([]);
  const authToken = localStorage.getItem('token');

  useEffect(() => {
    fetch(`https://capstone-2-ecleo.onrender.com/orders`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(
          data.map((order) => {
            return <OrderCard key={order._id} order={order} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1 className="text-center p-3 my-4">ORDERS:</h1>
      {order}
    </>
  );
}
