import { useEffect, useState } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
  // stored data type for user validation
  const navigate = useNavigate();

  // state hooks to store the values of the input fields
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [password1, setPassword1] = useState();
  const [password2, setPassword2] = useState();
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    // checks for duplicate email and proceeds to registration
    fetch(`https://capstone-2-ecleo.onrender.com/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire(
            'Registration Successful!',
            'Log in to proceed.',
            'success'
          );
          navigate('/login');
        } else {
          Swal.fire(
            'Registration Failed!',
            'Use a new email address.',
            'error'
          );
        }
      });

    // clear inputs
    setFirstName('');
    setLastName('');
    setEmail('');
    setPassword1('');
    setPassword2('');
  }

  // to enable submit button
  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  return (
    <>
      <h1 className="text-center p-3 my-4">REGISTER</h1>

      <Form
        className="col-md-8 col-lg-4 mx-auto"
        onSubmit={(e) => registerUser(e)}
      >
        <Row>
          <Form.Group
            as={Col}
            sm="12"
            md="6"
            className="mb-3"
            controlId="firstName"
          >
            <Form.Label>First Name:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter First Name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group
            as={Col}
            sm="12"
            md="6"
            className="mb-3"
            controlId="lastName"
          >
            <Form.Label>Last Name:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Last Name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>
        </Row>

        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address:</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
          <Form.Text className="text-muted">Passwords must match.</Form.Text>
        </Form.Group>

        {isActive ? (
          <Button className="float-end" variant="dark" type="submit">
            Register
          </Button>
        ) : (
          <Button className="float-end" variant="dark" type="submit" disabled>
            Register
          </Button>
        )}
      </Form>
    </>
  );
}
