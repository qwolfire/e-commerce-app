import { useEffect, useState } from 'react';
import { Container, Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';


export default function Products(){
    
    const [product, setProducts] = useState([])

    useEffect(() => {
        const authToken = localStorage.getItem('token')

        fetch(`https://capstone-2-ecleo.onrender.com/products/`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {

                return(
                    <ProductCard key={ product._id } product={product} />
                )
            }))
        })
    }, [])

    return(
        <>
            <h1 className='text-center p-3 my-4'>PRODUCTS:</h1>
            <Container className='p-3 my-4'>
                <Row className='justify-content-evenly'>
                    { product }
                </Row>
            </Container>
        </>
    )

};