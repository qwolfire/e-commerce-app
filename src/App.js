import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';

import './App.css';
import AdminProductAdd from './components/AdminProductAdd';
import AdminProductEdit from './components/AdminProductEdit';
import AppNavbar from './components/AppNavbar';
import AdminDb from './pages/AdminDb';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from './pages/Orders';
import OrderView from './components/OrderView';
import Product from './pages/Products';
import ProductView from './components/ProductView';
import Register from './pages/Register';

import { UserProvider } from './UserContext';

function App() {
  // State hook for the user state for a global scope / used to store the user information and will be used for validating
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  // clear localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // check if user information is stored on login and if localStorage is cleared on logout
  useEffect(() => {
    fetch(`https://capstone-2-ecleo.onrender.com/users/profile`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route
              path="/products/:id/edit"
              element={<AdminProductEdit />}
            ></Route>
            <Route path="/admin" element={<AdminDb />}></Route>
            <Route path="products/add" element={<AdminProductAdd />}></Route>
            <Route path="/" element={<Home />}></Route>
            <Route path="/login" element={<Login />}></Route>
            <Route path="/logout" element={<Logout />}></Route>
            <Route path="/orders" element={<Orders />}></Route>
            <Route path="/orders/:id" element={<OrderView />}></Route>
            <Route path="/products" element={<Product />}></Route>
            <Route path="/products/:id" element={<ProductView />}></Route>
            <Route path="/register" element={<Register />}></Route>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

// https://capstone-2-ecleo.onrender.com
// http://localhost:4000
