import React from 'react';


// data type of an object that can be used to store information that can be shared to other components
const UserContext = React.createContext();


export const UserProvider = UserContext.Provider;
export default UserContext;